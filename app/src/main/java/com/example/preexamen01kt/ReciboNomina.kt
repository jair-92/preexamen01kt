package com.example.preexamen01kt

class ReciboNomina {
    private var numRecibo = 0
    private var Nombre: String? = null
    private var horasTrabNormal = 0f
    private var horasTrabExtras = 0f
    private var puesto = 0
    private var impuestoPorc = 0f

    // Constructor ReciboNomina()
    fun ReciboNomina(numRecibo: Int, Nombre: String?, horasTrabNormal: Float, horasTrabExtras: Float,
                     puesto: Int, impuestoPorc: Float) {
        this.numRecibo = numRecibo
        this.Nombre = Nombre
        this.horasTrabNormal = horasTrabNormal
        this.horasTrabExtras = horasTrabExtras
        this.puesto = puesto
        this.impuestoPorc = impuestoPorc
    }

    // Métodos Set y Get
    fun setNumRecibo(numRecibo: Int) {
        this.numRecibo = numRecibo
    }

    fun getNumRecibo(): Int {
        return numRecibo
    }

    fun setNombre(nombre: String?) {
        Nombre = nombre
    }

    fun getNombre(): String? {
        return Nombre
    }

    fun setHorasTrabNormal(horasTrabNormal: Float) {
        this.horasTrabNormal = horasTrabNormal
    }

    fun getHorasTrabNormal(): Float {
        return horasTrabNormal
    }

    fun setHorasTrabExtras(horasTrabExtras: Float) {
        this.horasTrabExtras = horasTrabExtras
    }

    fun getHorasTrabExtras(): Float {
        return horasTrabExtras
    }

    fun setPuesto(puesto: Int) {
        this.puesto = puesto
    }

    fun getPuesto(): Int {
        return puesto
    }

    fun setImpuestoPorc(impuestoPorc: Float) {
        this.impuestoPorc = impuestoPorc
    }

    fun getImpuestoPorc(): Float {
        return impuestoPorc
    }

    // Métodos para cálculo de nómina
    fun calcularSubtotal(puesto: Int, horasTrabNormal: Float, horasTrabExtras: Float): Float {
        var pagoBase = 200f
        val subTotal: Float
        if (puesto == 1) {
            pagoBase = (pagoBase + pagoBase * 0.20).toFloat()
        } else if (puesto == 2) {
            pagoBase = (pagoBase + pagoBase * 0.50).toFloat()
        } else if (puesto == 3) {
            pagoBase = (pagoBase + pagoBase * 1.00).toFloat()
        } else {
            println("No es posible calcular")
        }
        subTotal = pagoBase * horasTrabNormal + pagoBase * horasTrabExtras * 2
        return subTotal
    }

    fun calcularImpuesto(puesto: Int, horasTrabNormal: Float, horasTrabExtras: Float):Float {
        return (calcularSubtotal(puesto, horasTrabNormal, horasTrabExtras) * 0.16).toFloat()
    }

    fun calcularTotal(
        puesto: Int,
        horasTrabNormal: Float,
        horasTrabExtras: Float
    ): Float {
        val subTotal = calcularSubtotal(puesto, horasTrabNormal, horasTrabExtras)
        val impuesto = calcularImpuesto(puesto, horasTrabNormal, horasTrabExtras)
        return subTotal - impuesto
    }

}