package com.example.preexamen01kt

import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import android.os.Bundle

class ReciboNominaActivity : AppCompatActivity() {
    private lateinit var txtNumRecibo: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtHorasNormal: EditText
    private lateinit var txtHorasExtra: EditText

    private lateinit var rbAuxiliar: RadioButton
    private lateinit var rbAlbañil: RadioButton
    private lateinit var rbIngObra: RadioButton

    private lateinit var lblUsuario: TextView
    private lateinit var txtSubtotal: EditText
    private lateinit var txtImpuesto: EditText
    private lateinit var txtTotalPagar: EditText

    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button

    private lateinit var reciboN: ReciboNomina


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibo_nomina)

        iniciarComponentes()
        // Obtener los datos del MainActiviy
        // Obtener los datos del MainActiviy
        var datos = intent.extras
        var nombre = datos!!.getString("nombre")
        lblUsuario.text = nombre.toString()

        btnCalcular.setOnClickListener { calcular() }

        btnLimpiar.setOnClickListener { limpiar() }

        btnRegresar.setOnClickListener { regresar() }

    }

    private fun iniciarComponentes() {
        // Relacionar los objetos
        txtNumRecibo = findViewById(R.id.txtNumRecibo)
        txtNombre = findViewById(R.id.txtNombre)
        txtHorasNormal = findViewById(R.id.txtHorasNormal)
        txtHorasExtra = findViewById(R.id.txtHorasExtra)
        txtSubtotal = findViewById(R.id.txtSubtotal)
        txtImpuesto = findViewById(R.id.txtImpuesto)
        txtTotalPagar = findViewById(R.id.txtTotalPagar)

        lblUsuario = findViewById(R.id.lblUsuario)

        rbAuxiliar = findViewById(R.id.rbAuxiliar)
        rbAlbañil = findViewById(R.id.rbAlbañil)
        rbIngObra = findViewById(R.id.rbIngObra)

        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)

        reciboN = ReciboNomina()

        deshabilitarTextos()
    }

    private fun calcular() {
        var puesto = 0
        var horasNormales = 0f
        var horasExtra = 0f
        var subtotal = 0f
        var impuesto = 0f
        var total = 0f

        if (rbAuxiliar.isChecked) {
            puesto = 1
        } else if (rbAlbañil.isChecked) {
            puesto = 2
        } else if (rbIngObra.isChecked) {
            puesto = 3
        }

        if (txtNumRecibo.text.toString().isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese su número de recibo", Toast.LENGTH_SHORT)
                .show()
        } else if (txtNombre.text.toString().isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese su nombre", Toast.LENGTH_SHORT).show()
        } else if (txtHorasNormal.text.toString().isEmpty() || txtHorasExtra.text.toString().isEmpty()) {
            if (txtHorasNormal.text.toString().isEmpty()) {
                Toast.makeText(this, "Por favor, ingrese las horas trabajadas", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Por favor, ingrese las horas extras trabajadas", Toast.LENGTH_SHORT).show()
            }
        }
        if (!txtHorasNormal.text.toString().isEmpty() && !txtHorasExtra.text.toString().isEmpty() && puesto != 0) {
            horasNormales = txtHorasNormal.text.toString().toFloat()
            horasExtra = txtHorasExtra.text.toString().toFloat()
            subtotal = reciboN.calcularSubtotal(puesto, horasNormales, horasExtra)
            impuesto = reciboN.calcularImpuesto(puesto, horasNormales, horasExtra)
            total = reciboN.calcularTotal(puesto, horasNormales, horasExtra)
            txtSubtotal.setText("" + subtotal)
            txtImpuesto.setText("" + impuesto)
            txtTotalPagar.setText("" + total)
        } else if (puesto == 0) {
            Toast.makeText(this, "Por favor, seleccione su puesto", Toast.LENGTH_SHORT).show()
        }
    }

    private fun limpiar() {
        txtNumRecibo.setText("")
        txtNombre.setText("")
        txtHorasNormal.setText("")
        txtHorasExtra.setText("")
        txtSubtotal.setText("")
        txtImpuesto.setText("")
        txtTotalPagar.setText("")
    }

    private fun regresar() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Recibo de nómina")
        confirmar.setMessage("Regresar al MainActivity")
        confirmar.setPositiveButton("Confirmar") { dialog, which -> finish() }
        confirmar.setNegativeButton("Cancelar") { dialog, which ->
            // No se realiza nada
        }
        confirmar.show()
    }

    private fun deshabilitarTextos() {
        txtSubtotal.isFocusable = false
        txtImpuesto.isFocusable = false
        txtTotalPagar.isFocusable = false
    }

}