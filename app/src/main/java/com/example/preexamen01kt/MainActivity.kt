package com.example.preexamen01kt

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    private lateinit var txtNombreTrabajador : EditText
    private lateinit var lblNombreTrabajador : TextView

    private lateinit var btnEntrar : Button
    private lateinit var btnSalir : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        iniciarComponentes()
        btnEntrar.setOnClickListener { entrar() }

        btnSalir.setOnClickListener { salir() }

    }

    private fun iniciarComponentes() {
        lblNombreTrabajador = findViewById(R.id.lblNombreTrabajador)
        txtNombreTrabajador = findViewById(R.id.txtNombreTrabajador)
        btnEntrar = findViewById(R.id.btnEntrar)
        btnSalir = findViewById(R.id.btnSalir)
    }

    private fun entrar() {
        var strNombre: String
        strNombre = applicationContext.resources.getString(R.string.nombre)
        if (strNombre == txtNombreTrabajador.text.toString().trim()) {
            // Hacer el paquete para enviar información
            var bundle = Bundle()
            bundle.putString("nombre", txtNombreTrabajador.text.toString())

            // Crear el intent para llamar a otra actividad
            val intent = Intent(this@MainActivity, ReciboNominaActivity::class.java)
            intent.putExtras(bundle)

            // Iniciar la actividad esperando o no una respuesta
            startActivity(intent)
        } else {
            Toast.makeText(this.applicationContext, "El usuario no es válido", Toast.LENGTH_SHORT).show()
        }
    }

    private fun salir() { finish() }

}